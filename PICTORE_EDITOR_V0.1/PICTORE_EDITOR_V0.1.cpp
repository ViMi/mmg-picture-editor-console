﻿#include "pch.h"
#include <iostream>
#include <conio.h>
#include <Windows.h>
#include <mmsystem.h>
#include <pthread.h>


#define BEEP() _asm { \
                _asm mov bx,0      \
                _asm mov ax, 0E07h \
                _asm int 10h       \
                          }

#define ALA \
12

#define DEBUG 1




#define SOLID_MATERIAL 101

#define SOMETHING(x) x##_Type

#define SOTH(x) #x

typedef struct ListNode {
	short value;
	ListNode *next;
}Node;

struct MMF {
	HANDLE hFile;
	HANDLE hMappiing;
	short *dataptr;
};



#define NL		printf("\n");
#define TB		printf("\t");
#define CR		printf("\r");
#define STOP(x) printf("Stage>>[%s]\n",x);

#define ANGLE_0 0
#define ANGLE_1 4
#define ANGLE_5 20
#define ANGLE_10 40
#define ANGLE_30 120
#define ANGLE_45 180
#define ANGLE_60 240
#define ANGLE_75 300
#define ANGLE_90 360
#define ANGLE_180 720
#define ANGLE_360 1440

#define TRANSPONENT 334


float fPlayerX = 35.0;
float fPlayerY = 24.0f;
float fPlayerA = 0.0f;			
float fFOV = 3.14159f / 4.0f;	
float fDepth = 40.0f;			
float fSpeed = 0.5f;			
const int nScreenWidth = 120;
const int nScreenHeight = 40;		
int nMapWidth = 40;			
int nMapHeight = 40;






short DoorTexture[40][10] = {

{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0},

};
short mapWorld[][16] = {

{1,1,1,1,1,1,1,1,1,1,1,1},
{1,334,334,334,334,334,334,334,334,334,334,1},
{1,334,334,334,334,334,5,334,334,334,334,1},
{1,334,334,334,334,334,334,334,334,334,334,1},
{1,334,334,334,334,334,334,334,334,334,334,1},
{1,334,334,334,334,334,334,334,334,334,334,1},
{1,334,334,334,334,334,334,334,334,334,334,1},
{1,334,334,334,334,334,12,334,334,334,334,1},
{1,334,334,334,334,334,334,334,334,334,334,1},
{1,334,334,334,334,334,334,334,334,334,334,1},
{1,334,334,334,1,334,334,334,334,334,334,1},//10,7
{1,334,334,334,334,334,334,334,334,334,334,1},
{1,334,334,334,334,334,334,334,334,334,334,1},//12
{1,334,334,1,334,334,334,334,334,334,334,1},
{1,334,334,12,12,12,12,12,12,12,12,12},
{1,1,1,1,1,1,1,1,1,1,1,1},

};
short TextureOfWhiteWall[40][10] = {

{7, 7, 7, 7, 7, 7, 7, 7, 7, 7},
{7, 7, 7, 7, 7, 7, 7, 7, 7, 7},
{7, 7, 7, 7, 7, 7, 7, 7, 7 ,7},
{7, 7, 7, 7, 7, 7, 7, 7, 7, 7},
{7 ,7, 7, 7, 3, 7, 7, 7, 7, 7},
{7, 7, 7, 3, 3, 3, 7, 7, 7, 7},
{7, 7,12,12,12,12,12,12, 7, 7},
{7, 7, 7,12,12,12,12, 7, 7, 7},
{7, 7, 7, 7,12,12, 7, 7, 7, 7},
{7, 7, 7, 7,12,12, 7, 7, 7, 7},
{7, 7, 7, 7,12,12, 7, 7, 7, 7},
{7, 7, 7, 7,12,12, 7, 7, 7, 7},
{7, 7, 7, 7,12,12, 7, 7, 7, 7},
{12, 7, 7, 7,12,12, 7, 7, 7,12},
{7,12, 7, 7,12,12, 7, 7,12, 7},
{7, 7,12, 7,12,12, 7,12, 7, 7},
{7, 7, 7,12,12,12,12, 7, 7, 7},
{7, 7,12, 7,12,12, 7,12, 7, 7},
{7,12, 7, 7,12,12, 7, 7,12, 7},
{12,7, 7, 7,12,12, 7, 7, 7,12},
{7, 7, 7, 7,12,12, 7, 7, 7, 7},
{7, 7, 7, 7,12,12, 7, 7, 7, 7},
{7, 7, 7, 7,12,12, 7, 7, 7, 7},
{7, 7, 7, 7,12,12, 7, 7, 7, 7},
{7, 7, 7, 7,12,12, 7, 7, 7, 7},
{7, 7, 7, 7,12,12, 7, 7, 7, 7},
{7, 7, 7, 7,12,12, 7, 7, 7, 7},
{7, 7, 7, 7,12,12, 7, 7, 7, 7},
{7, 7, 7, 7,12,12, 7, 7, 7, 7},
{7, 7, 7,12,12,12,12, 7, 7, 7},
{7, 7,12,12,12,12,12,12, 7, 7},
{7, 7, 7, 3, 3, 3, 7, 7, 7, 7},
{7, 7, 7, 7, 3, 7, 7, 7, 7, 7},
{7, 7, 7, 7, 7, 7, 7, 7, 7, 7},
{7, 7, 7, 7, 7, 7, 7, 7, 7, 7},
{7, 7, 7, 7, 7, 7, 7, 7, 7, 7},
{7, 7, 7, 7, 7, 7, 7, 7, 7, 7},
{7, 7, 7, 7, 7, 7, 7, 7, 7, 7},
{7, 7, 7, 7, 7, 7, 7, 7, 7, 7},
{7, 7, 7, 7, 7, 7, 7, 7, 7, 7},
};

float *x_steps; //Массивы инициализируемые значениями шага (Основаном на тригонометрических функциях (которые соотносим с декартовой (прямоуголоиной системой кординат двумерной карты через тригоном. круг)
float *y_steps;
float *z_steps;
float fPlayerZ = 1.0;


char ANIMATION = 0;

static short ROWS;
static short COLUMNS;
static short **picture;
static char name_of_file_to_add[100];
static short *Animation;
static FILE *b;

char music=1;
pthread_t thread;

char ObjectArrayNum[16] = {120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,199};

char ObjectArray[16][50]{
{"Door"},
{"Terminal"},
{"TerminalPolice"},
{"TerminalCorporation"},
{"TerminalImperor"},
{"Bad"},
{"Car"},
{"Bath"},
{"TerminalWall"},
{"ImperorWall"},
{"MainComputer"},
{"Planet(1)"},
{"Planet(2)"},
{"Planet(3)"},
{"Char"},
{"s"},
};


MMF mapStruct;


int main(int argc, char *argv[]);
void MainMenu(void);
void Return(void);
void Sound(void);
void Pause(void);
void ErrorOfEnter(void);
void Black(void);
void Painter(int position_ROWS, int position_COLUMNS);
void Painter(int position_ROWS, int position_COLUMNS, int z);
void OpenFile(const char type[], char name[]);
void Save(void);
void EditPicture(char type);
void Enter(const char type[], short *var);
void CreateNewPicture(void);
void FileList(char type);
void Read(const char type='P');
void GetMetaData(const char type);
void Return(void);
void Input(short *chose);
void GenerateCanvas(void);
char* IntToString(int n);
int StringToInt(const char*s);
char* DoubleToStr(double n, int acuracy);
void AnimationPlay(short StartROWS, short StartCOLUMNS, short Collor);
void AnimationInit(short background, char *nameOfBackground);
short AnimHelp(Node *list, short Color, short i, short j, short *AnimaCount);
void GenerateAnimation(void);
void PainterTransfer
(int position_ROWS, int position_COLUMNS, const char type, short typeOfCollor,int z=0);
void RefreshPicture(int position_ROWS, int position_COLUMNS, short typeOfCollor);
void AddNode(Node *list, short val, short counter);
short GetNode(Node *list, short count);
short Input(void);
void EditPicture(char type);
void Frame(int a, int b);
void ColorUnit(short num,int sizeOfField=0);
//void GenerateTable(void);
void* ObjectManager(unsigned char i);
void Action(char x, char y, char z = 1);
void Render3d(float fPlayerX, float fPlayerY, float fPlayerZ);
void PrintDisplay(void);
//void RegistratorOfCollision
//(float fPlayerX, float fPlayerY, float y_step, float x_step, float fPlayerZ, int x, int yPosition = 0);
//void MainLoopOfRenderer(float fPlayerX, float fPlayerY, float fPlayerZ,int fPlayerA);






/*
Ax+By+Cz+D = 0, где A, B, C — координаты нормали к плоскости, а D — расстояние до начала координат.
*/



typedef struct VolumePoint {
	short x;
	short y;
	short z;
}VP;

short ***WorldBox;

short ZDimension;

void CreateWorld(void) {
	short x, y, z;
	short **secondPointer, *firstpointer;


	_asm {
		//Проверка переполнение при присвоении
	}

	printf("Enter Size Of World X");
	scanf("%d", &COLUMNS);
	printf("Enter Size of World y");
	scanf("%d", &ROWS);
	printf("Enter Size of World Z");
	scanf("%d", &ZDimension);

	WorldBox = (short***)calloc(COLUMNS +(ROWS*COLUMNS *sizeof(short*))+(ROWS*COLUMNS*ZDimension *sizeof(short)), sizeof(short**));
	secondPointer = (short**)(WorldBox + COLUMNS);
	firstpointer = (short*)(WorldBox + COLUMNS + COLUMNS * ROWS);


	for (register short i = 0; i < x; i++) {
		WorldBox[i] = secondPointer + i * ROWS;
		for (register short j = 0; j < ROWS; j++) {
			WorldBox[i][j] = firstpointer + j * (ROWS * ZDimension)+(i*ROWS*ZDimension*COLUMNS);
		}
	}

}

void ObjectManager(int i, short *WorldBox) {
	//OpenFile
	_asm {
		//Открытие файла и чтение из него
	}


	fseek(b, sizeof(char)*i, SEEK_SET);
	fread(WorldBox, sizeof(char), 1, b);
}

void RefreshBuffer(void) {

	for (register char x = 0; x < nScreenHeight; x++) {
		for (register char y = 0; y < nScreenWidth; y++) {
		//	if (ARRAY[x][y] != ARRAYSecond[x][y])
			//	RefreshPicture(x, y, ARRAYSecond[x][y]);
		}
	}

}

void* ObjectManager(unsigned char i) {

	switch (i) {

	case 0: {return (void*)0; }break;//Black Row
	case 1: {return (void*)1; }break;//Red Row
	case 2: {return (void*)2; }break;//Green Row
	case 3: {return (void*)3; }break;//Yellow Row
	case 4: {return (void*)4; }break;//Blue Row
	case 5: {return (void*)5; }break;//Purple Row
	case 6: {return (void*)6; }break;//Light_blue row
	case 7: {return (void*)7; }break;//White Row
	case 11: {return (void*)11; }break;//Mauve Row
	case 12: {return (void*)12; }break;//Grey Row
	case 13: {return (void*)13; }break;//Light_Grey Row
	case 14: {return (void*)14; }break;//Light_green Row
	case 15: {return (void*)15; }break;//Light_light_bue Row
	case 16: {return (void*)16; }break;//Light_Red Row
	case 17: {return (void*)17; }break;//Light_Yllow Row
	case 18: {return (void*)18; }break;//Light_White Row


	case 8: {}break;
	case 9: {}break;
	case 10: {return TextureOfWhiteWall; }break;
	case 19: {}break;
	case 20: {}break;
	case 21: {}break;
	case 22: {}break;
	case 23: {}break;
	case 24: {}break;
	case 25: {}break;
	default: {ErrorOfEnter(); }
	}

}

void CollisionSection(float fPlayerX, float fPlayerY, float fPlayerZ, float x_step, float y_step, float z_step, int x, int yPos = 0) {

	float fDistanceToWall = 0.0f;
	char typeOfObject;
	char PositionOfTexture;
	short **ObjecctSprite;

	for (register short y = yPos; y < nScreenHeight; y++) {

		while (1) {
			fDistanceToWall += 0.1;
			int	nTestX = (int)(fPlayerX + x_step * fDistanceToWall);
			int nTestZ = (int)(fPlayerZ + z_step * fDistanceToWall);
			int	nTestY = (int)(fPlayerY + y_step * fDistanceToWall);

			PositionOfTexture = ((fPlayerX + x_step * fDistanceToWall) * 10) - (nTestX * 10);

			if (WorldBox[nTestZ][nTestY][nTestX] != 334) {

				//if (WORLD3d[nTestZ][nTestY][nTestX] == 12) {
				//	ObjecctSprite = (short**)ObjectManager(WorldBox[nTestZ][nTestY][nTestX]);
				//	if (ObjecctSprite[y][PositionOfTexture] == 334)CollisionSection(nTestX, nTestY, nTestZ, x_step, y_step, z_step, x, y);
				//	else {
				//		ARRAY[x][y] = ObjecctSprite[y][PositionOfTexture];
				//	}
				//}
				//else {
				//	ARRAY[x][y] = WORLD3d[nTestZ][nTestY][nTestX];
				//}

			}

			printf("X=%3.2f ... Y=%3.2f ... Z=%3.2f ... typeOfObject=%d .... IndexOfColumn=%d\n",
				(fPlayerX + x_step * fDistanceToWall), (fPlayerY + x_step * fDistanceToWall), (fPlayerZ + z_step * fDistanceToWall), typeOfObject, PositionOfTexture);
		}

	}
}

void Render3d(float fPlayerX, float fPlayerY, float fPlayerZ) {


	for (register short x = 0; x < nScreenWidth; x++) {

		CollisionSection(fPlayerX, fPlayerY, fPlayerZ, x_steps[x], y_steps[x], z_steps[x], x);
	}


}

void Action(char z, char y, char x) {
	//switch (WORLD3d[z][y + 1][x]) {
	//case 1: { }break; //Home Terminal
	//case 2: { }break; //System Terminal
	//case 3: {WORLD3d[z][y + 1][x] = 26; ANIMATION = 10; }break; //Door(Open)
	//case 4: {WORLD3d[z][y + 1][x] = 25; ANIMATION = 10;  }break;//Door(Close)
	//case 5: { }break;
	//}
}
/*
void Game(void) {

	float fPlayerZ = 1.0;

	float fPlayerA = ANGLE_0;
	while (1) {
		system("cls");
		switch (_getch()) {
		case 'a':
		case 'A': {if ((fPlayerA -= ANGLE_5) < 0)fPlayerA = ANGLE_360; }break;
		case 'd':
		case 'D': {if ((fPlayerA += ANGLE_5) >= ANGLE_360)fPlayerA = ANGLE_0; }break;

		case 'w':
		case 'W': {

			if (WorldBox[(int)fPlayerZ][(int)fPlayerY][(int)fPlayerX] == 334) {
				fPlayerX += sinf(fPlayerA) * .5f;
				fPlayerY += cosf(fPlayerA) * .5f;
			}

		}break;
		case 's':
		case 'S': {

			if (WorldBox[(int)fPlayerZ][(int)fPlayerY][(int)fPlayerX] == 334)
				fPlayerX -= sinf(fPlayerA) * .5f;
			fPlayerY -= cosf(fPlayerA) * .5f;

		}break;


		case 'u':
		case 'U': {
			fPlayerZ += 1;
		}break;

		case 'i':
		case 'I':
		{
			fPlayerZ -= 1;

		}break;

		case 'e':
		case 'E': {

			if (WorldBox[(int)fPlayerZ][(int)fPlayerY][(int)fPlayerX] != 334)
				Action((int)fPlayerZ, (int)fPlayerY, (int)fPlayerX);

		}break;
		}

		GenerateTable();
		printf("ANGLE -%d\n", fPlayerA);
		MainLoopOfRenderer(fPlayerX, fPlayerY, fPlayerZ, fPlayerA);
		PrintDisplay();
	}

}
*/

/*
void GenerateTable(void) {

	//
	//Ниже приведен очень важный цикл дело в том что пуская произвольное значение луча мы получаем
	//проблему а именно мы не можем нормально текстурировать кубы так как просто не знаем какой шаг нужен для них
	//Так что цикл ниже призван решит сразу несколько проблем во перввых несмотря на то что с точки зрения произволдительности
	//данная программа вызывает большие вопросы в основном по тому что я тупой однако мы все же постараемся избежать
	//порождения лишних сущьностей там где это не надо (конечно стопроцентно веровать в бога как предполагал автор изречения мы не будем но тем не менее
	//сдравое зерно тут есть)
	//Тем не менее погнали
	//

	y_steps = (float*)calloc(ANGLE_360 + 1, sizeof(float));
	x_steps = (float*)calloc(ANGLE_360 + 1, sizeof(float));
	z_steps = (float*)calloc(ANGLE_180 + 1, sizeof(float));

	float fRayAngle;

	for (register short x = 0; x <= ANGLE_360; x++) {

		//	fRayAngle = (fPlayerA - fFOV / 2.0f) + ((float)x / (float)nScreenWidth)*fFOV;

		fRayAngle = (3.272e-4) + x * 2 * 3.141592654 / ANGLE_360;
		y_steps[x] = cosf(fRayAngle);
		x_steps[x] = sinf(fRayAngle);
	}

	for (register short x = 0; x <= ANGLE_180; x++)
		z_steps[x] = sin(fRayAngle);


}
*/
void PrintDisplay(void) {

	if (DEBUG) {
		printf("X=%3.2f, Y=%3.2f, A=%3.2f\n", fPlayerX, fPlayerY, fPlayerA);
	}

	//Выводим кадр на консоль
	for (register char y = 0; y < nScreenHeight; y++) {
		for (register char x = 0; x < nScreenWidth; x++) {
		//	ColorUnit(ARRAY[y][x]);
		}
		printf("\n");
	}
	_getch();

}

/*
void RegistratorOfCollision
(float fPlayerX, float fPlayerY, float y_step, float x_step, float fPlayerZ, int x, int yPosition) {

	bool bHitWall = false;
	float testXTexture = 0.0;

	float fDistanceToWall = 0.0f;
	int nTestX = 0;
	int nTestY = 0;

	unsigned short nCeiling = 0;
	unsigned short nFloor = 0;

	unsigned short typeOfObject = 0;
	unsigned short nShade = 0, nShadeCeilig = 0, nShadeFloor = 0;

	short **Texture;

	while (!bHitWall && (fDistanceToWall) < fDepth)
	{

		fDistanceToWall += 0.01f;

		nTestX = (int)(fPlayerX + x_step * (fDistanceToWall));
		nTestY = (int)(fPlayerY + y_step * (fDistanceToWall));

		testXTexture = (fPlayerX + x_step * (fDistanceToWall)); //Данная переменная в отличии от первой хранит данные в float это нам потребуется для наложения текстуры 

	   // Проверяем эти точки 
		if (nTestX < 0 || nTestX >= nMapWidth || nTestY < 0 || nTestY >= nMapHeight) {
			bHitWall = true;
			fDistanceToWall = fDepth; //Максимальная дальность
		}

		else
		{

			if ((typeOfObject = WorldBox[(int)fPlayerZ][nTestY][nTestX]) != TRANSPONENT)
			{


				bHitWall = true;

				float Edge[8];
				int c = 0;
				for (register char tx = 0; tx < 2; tx++) {
					for (register char ty = 0; ty < 2; ty++)
					{
						float vy = (float)nTestY + ty - fPlayerY;
						float vx = (float)nTestX + tx - fPlayerX;

						float d = sqrt(vx*vx + vy * vy); //(Расстояние) Нужно для определения где находится ближняя грань
						float dot = ((x_step * vx) / d) + ((y_step * vy) / d); //Угол между векторами минималень когда скаляр векторов минимален (Нужно для определения видим мы грань или еще пока не)

						Edge[c] = d;
						c++;
						Edge[c] = dot;
						c++;

					}
				}



				int flag = 0;
				do {

					flag = 0;

					for (int i = 0; i <= 7; i += 2)
					{

						if (Edge[i] < Edge[(i + 2)]) {
							float temp = Edge[(i + 1)];
							Edge[(i + 1)] = Edge[(i + 3)];
							Edge[(i + 3)] = temp;

							temp = Edge[i];
							Edge[i] = Edge[i + 2];
							Edge[i + 2] = temp;


							flag = 1;

						}

					}
				} while (flag == 1);


				float fBound = 0.008;
				if (acos(Edge[5]) < fBound) typeOfObject = 0;
				if (acos(Edge[7]) < fBound) typeOfObject = 0;

			}

		}

	}


	nCeiling = ((float)(nScreenHeight / 2.0) - nScreenHeight / ((float)fDistanceToWall));
	nFloor = (nScreenHeight - nCeiling);


	if (typeOfObject == 12)
		**Texture = (short)ObjectManager(typeOfObject);

	char A = 0;
	char B = 0;

	fDistanceToWall = 0.0f;

	short UP = (short)fPlayerZ + 1;
	short DOWN = (short)fPlayerZ - 1;
	char a = 0;

	while (!a) {

		fDistanceToWall += 0.1f;
		nTestY = (int)(fPlayerY + y_step * fDistanceToWall);

		if (((nShadeCeilig = WorldBox[UP][nTestY][nTestX]) != TRANSPONENT)) {
			B = 1;
		}
		if ((nShadeFloor = WorldBox[DOWN][nTestY][nTestX]) != TRANSPONENT) {
			A = 1;
		}
		a = (B != 0 && A != 0);
	}

	if (typeOfObject < 10)
		nShade = (short)(ObjectManager(typeOfObject));

	//Собственно отрисовываем в масиив строку изображения	
	int zetta = 0;
	int lala = 0;
	int ala = 0;

	if ((nFloor - nCeiling) < 40) {
		lala = (nFloor - nCeiling) / 2;
		ala = 40 - (nFloor - nCeiling);
	}

	//ANIMATION
	if (ANIMATION < 10) {
		if (nShade == 25) {
			for (register char y = 0; y < nScreenHeight; y++)
				Texture[y][ANIMATION] = 334;
			ANIMATION--;
			if (!ANIMATION)WORLD3d[(int)fPlayerZ][(int)fPlayerY][(int)fPlayerX] = 26;
		}
		if (nShade == 26) {
			short **tmpTexture = (short**)ObjectManager(nShade - 1);
			for (register char y = 0; y < nScreenHeight; y++)
				Texture[y][ANIMATION] = tmpTexture[y][ANIMATION];
			ANIMATION++;
			if (ANIMATION)WORLD3d[(int)fPlayerZ][(int)fPlayerY][(int)fPlayerX] = 25;
		}
	}
	//////

	int indexOfColumn = ((testXTexture * 10.) - nTestX * 10);

	for (int y = yPosition; y < nScreenHeight; y++)
	{
		if (y <= nCeiling)
			ARRAY[y][x] = nShadeCeilig;
			//ARRAY ? ARRAY[y][x] = nShadeCeilig : ARRAYSecond[y][x] = nShadeCeilig;

		else if (y > nCeiling && y <= nFloor) {
			if (nShade == 7 || nShade == 0 || nShade == 1 || nShade == 3)
				ARRAY[y][x] = nShade;
			//	ARRAY ? ARRAY[y][x] = nShade : ARRAYSecond[y][x] = nShade;

			if (nShade == 4) {
				if ((nFloor - nCeiling) < 40) {

					if (TextureOfWhiteWall[zetta][indexOfColumn] == 334) {


						RegistratorOfCollision
						(fPlayerX, fPlayerY, y_step, x_step, fPlayerZ, x, y);

					}
					else {
						ARRAY[y][x] = TextureOfWhiteWall[zetta][indexOfColumn];
						zetta++;
						if (zetta == lala)zetta = zetta + ala;
					}



				}
				else {
					if (TextureOfWhiteWall[y][indexOfColumn] == TRANSPONENT) {

						RegistratorOfCollision
						(fPlayerX, fPlayerY, y_step, x_step, fPlayerZ, x, y);

					}
					else ARRAY ? ARRAY[y][x] = TextureOfWhiteWall[y][indexOfColumn] : ARRAYSecond[y][x] = TextureOfWhiteWall[y][indexOfColumn];
				}
			}
		}

		else
		{
			float b = 1.0f - (((float)y - nScreenHeight / 2.0f) / ((float)nScreenHeight / 2.0f));
			ARRAY ? ARRAY[y][x] = nShadeFloor : ARRAY[y][x] = nShadeFloor;
		}
	}


}
*/

/*
void MainLoopOfRenderer(float fPlayerX, float fPlayerY, float fPlayerZ, int fPlayerA) {

	for (int x = 0; x < nScreenWidth; x++) {
		RegistratorOfCollision
		(fPlayerX, fPlayerY, y_steps[fPlayerA], x_steps[fPlayerA], fPlayerZ, x);
		if (++fPlayerA >= ANGLE_360)
		{
			fPlayerA = 0; //Заходим на круг
		}
	}
	//if (ARRAY)RefreshBuffer();
}
*/













void AnimationInit(short background, char *nameOfBackground) {
	system("cls");
	short CountElements = 0;
	if (b)fclose(b);
	OpenFile("rb", name_of_file_to_add);
	if (Animation)free(Animation);
	fread(&CountElements, sizeof(short), 1, b);
    Animation = (short*)calloc(CountElements*3, sizeof(short));
	for (int i = 0; i < CountElements*3; i++)
		fread(&Animation[i], sizeof(short), 1, b);
	fclose(b);


	if (background == 1) {

		for (register int i = 0; i < 100; i++)
			name_of_file_to_add[i] = ' ';

		for (register int i = 0; i < strlen(name_of_file_to_add); i++)
			name_of_file_to_add[i] = nameOfBackground[i];
		Read();
		Painter(ROWS, COLUMNS);
	}

		for (register int j = 0; j < CountElements*3; j+=3)
		{
				AnimationPlay
 		(Animation[j],Animation[j+1],Animation[j+2]);
				Sleep(60);
		}
		printf("\x1b[%s;0H", IntToString(ROWS+10));
}

void AnimationPlay(short StartROWS,short StartCOLUMNS,short Collor) {
	PainterTransfer(StartROWS,StartCOLUMNS,'r',Collor);
}

void GenerateAnimation(void) {
	EditPicture('A');
}

void Sound(void) {
	Beep(240, 200);
}

void Pause(void) {
	_getch(); _getch();
}

void ErrorOfEnter(void) {
	TB; Beep(256, 200); perror("\x1b[0;31;47mError of enter Repeat pictureou request\x1b[0;37;40m"); NL;
}

void Black(void) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, (WORD)((0 << 4) | 7));
}

void ColorUnit(short num,int sizeOfField) {

	if (num == 0)printf("\x1b[0;30;40m%*c\x1b[0;37;40m", sizeOfField, 'a');//Черный 
	if (num == 1)printf("\x1b[0;31;41m%*c\x1b[0;37;40m", sizeOfField, 'a');//Красный
	if (num == 2)printf("\x1b[0;32;42m%*c\x1b[0;37;40m", sizeOfField, 'a');//Зеленый
	if (num == 3)printf("\x1b[0;33;43m%*c\x1b[0;37;40m", sizeOfField, 'a');//Желтый
	if (num == 4)printf("\x1b[0;34;44m%*c\x1b[0;37;40m", sizeOfField, 'a');//Синий
	if (num == 5)printf("\x1b[0;35;45m%*c\x1b[0;37;40m", sizeOfField, 'a');//Пурпурный
	if (num == 6)printf("\x1b[0;36;46m%*c\x1b[0;37;40m", sizeOfField, 'a');//"Голубой, голубой не хотим играть с тобой" (Вы не находите чт мультфильм Голубой Щенок в нынешних реалиях несколько своебразно воспринимается ?)
	if (num == 7)printf("\x1b[0;37;47m%*c\x1b[0;37;40m", sizeOfField, 'a');//Белый
	if (num == TRANSPONENT)printf("\x1b[0;37;40m%*c\x1b[0;37;40m", sizeOfField, 'n');
	if (num == 11) {
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleTextAttribute(hConsole, (WORD)((5 << 4) | 5)); printf("%*c", sizeOfField, 'a');
		Black();//Лиловый
	}
	if (num == 12) {
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleTextAttribute(hConsole, (WORD)((8 << 4) | 8)); printf("%*c", sizeOfField, 'a');
		Black(); //Серый
	}
	if (num == 13) {
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleTextAttribute(hConsole, (WORD)((9 << 4) | 9)); printf("%*c", sizeOfField, 'a');
		Black(); //Светло-синий
	}
	if (num == 14) {
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleTextAttribute(hConsole, (WORD)((10 << 4) | 10)); printf("%*c", sizeOfField, 'a');
		Black();//Светло-зеленый
	}
	if (num == 15) {
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleTextAttribute(hConsole, (WORD)((12 << 4) | 12)); printf("%*c", sizeOfField, 'a');
		Black();//светло-голубой
	}
	if (num == 16) {
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleTextAttribute(hConsole, (WORD)((13 << 4) | 13)); printf("%*c", sizeOfField, 'a');
		Black();//светло-красный
	}
	if (num == 17) {
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleTextAttribute(hConsole, (WORD)((14 << 4) | 14)); printf("%*c",sizeOfField,'a');
		Black();//светло-желты
	}
	if (num== 18) {
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleTextAttribute(hConsole, (WORD)((15 << 4) | 15)); printf("%*c", sizeOfField, 'a');
		Black();//ярко-белый
	}
	if (num == SOLID_MATERIAL) {
		printf("\x1b[0;30;40m%*c\x1b[0;37;40m", sizeOfField, 'a');
	}
	if (num == 120) {
		printf("\x1b[0;37;40m%*c\x1b[0;37;40m", sizeOfField, 'd');
	}
	if (num == 121) {
		printf("\x1b[0;37;40m%*c\x1b[0;37;40m", sizeOfField, 't');
	}
	if (num == 122) {
		printf("\x1b[0;37;40m%*c\x1b[0;37;40m", sizeOfField, 'y');
	}
	if (num == 123) {
		printf("\x1b[0;37;40m%*c\x1b[0;37;40m", sizeOfField, 'u');
	}
	if (num == 124) {
		printf("\x1b[0;37;40m%*c\x1b[0;37;40m", sizeOfField, 'i');
	}
	if (num == 125) {
		printf("\x1b[0;37;40m%*c\x1b[0;37;40m", sizeOfField, 'k');
	}
	if (num == 126) {
		printf("\x1b[0;37;40m%*c\x1b[0;37;40m", sizeOfField, 'b');
	}
	if (num == 127) {
		printf("\x1b[0;37;40m%*c\x1b[0;37;40m", sizeOfField, 'l');
	}
	if (num == 128) {
		printf("\x1b[0;37;40m%*c\x1b[0;37;40m", sizeOfField, 'z');
	}
	if (num == 129) {
		printf("\x1b[0;37;40m%*c\x1b[0;37;40m", sizeOfField, 'i');
	}
	if (num == 130) {
		printf("\x1b[0;37;40m%*c\x1b[0;37;40m", sizeOfField, 'm');
	}
	if (num == 131) {
		printf("\x1b[0;37;40m%*c\x1b[0;37;40m", sizeOfField, 'p');
	}

		if (num == 132) {
			printf("\x1b[0;37;40m%*c\x1b[0;37;40m", sizeOfField, 'p');
	}

		if (num == 133) {
			printf("\x1b[0;37;40m%*c\x1b[0;37;40m", sizeOfField, 'p');
	}

		if (num == 134) {
			printf("\x1b[0;37;40m%*c\x1b[0;37;40m", sizeOfField, 'a');
	}

}




int create = 0;

void PainterTransfer
(int position_ROWS, int position_COLUMNS, const char type, short typeOfCollor,int z) {
	Frame(position_ROWS, position_COLUMNS);
	if (type == 'w') {
		Painter(position_ROWS, position_COLUMNS, z);
	}
	if (type == 's') {
		Painter(position_ROWS, position_COLUMNS);
	}
	if (type == 'r') {
		RefreshPicture(position_ROWS, position_COLUMNS, typeOfCollor);
	}
}

void inline Painter(int position_ROWS,int position_COLUMNS) {
	
	for (register int i = 0; i < ROWS; i++) {
				for (register int j = 0,c=1; j < COLUMNS; j++,c+=3) {
					printf("\x1b[%s;%sH", IntToString(i+1), IntToString(j+3+c));
					ColorUnit(picture[i][j],4);
				}
			NL;
		}
	printf("ROWS -- %d >>>  COLUMNS -- %d >>> Name -- %s", ROWS, COLUMNS, name_of_file_to_add); NL
		for (register int i = 0; i < ROWS; i++) {
			for (register int j = 0, c = 1; j < COLUMNS; j++, c += 3) {
				printf("%d", picture[i][j]);
			}
			NL;
		}

}

void inline Painter(int position_ROWS, int position_COLUNS, int ZDimension) {

	for(register int z=ZDimension;z<(ZDimension+1);z++)
		for(register int y=0;y<ROWS;y++)
			for (register int x = 0,c=1; x < COLUMNS; x++,c+=3) {
				printf("\x1b[%s;%sH", IntToString(y + 1), IntToString(x + 3 + c));
				ColorUnit(WorldBox[z][y][x],4);
			}
}

void Frame(int position_ROWS, int position_COLUMNS) {

	for (register int i = 0; i <= ROWS; i++) {
		if (i != ROWS) {
			 if (i == position_ROWS)printf("\x1b[%s;0H\x1b[0;33;40m%-3d\x1b[0;37;40m", IntToString(i+1), i);
			else printf("\x1b[%s;0H\x1b[0;37;40m%-3d\x1b[0;37;40m", IntToString(i+1), i);
		}
		if (i == ROWS)
		{
			for (register int j = 0,c=1; j < COLUMNS; j++,c+=3) {
				if (j == position_COLUMNS)printf("\x1b[%s;%sH\x1b[0;33;40m%-3d\x1b[0;37;40m", IntToString(ROWS+1), IntToString(j+3+c), j);
				else printf("\x1b[%s;%sH\x1b[0;37;40m%-3d\x1b[0;37;40m", IntToString(ROWS+1), IntToString(j+3+c), j);
			}
		}
	}
}

void RefreshPicture
(int position_ROWS, int position_COLUMNS, short typeOfCollor) {
	printf("\x1b[%s;%sH", IntToString(position_ROWS+1), IntToString(position_COLUMNS + 4 + ((position_COLUMNS*3))));
	ColorUnit(typeOfCollor,4);
}








MMF mmf;
void freeMap() {
	UnmapViewOfFile(mmf.dataptr);
	CloseHandle(mmf.hMappiing);
	CloseHandle(mmf.hFile);
}



struct FileMapping {
	HANDLE hFile;
	HANDLE hMapping;
	size_t fsize;
	unsigned char* dataPtr;
};

int CreateFileMap() {

	freeMap();
	mmf.hFile = CreateFile(name_of_file_to_add, GENERIC_ALL, 0, nullptr,
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL, nullptr);
	if (mmf.hFile == INVALID_HANDLE_VALUE) {
	
		return 1;
	}
	
	DWORD dwFileSize = GetFileSize(mmf.hFile, nullptr);

	if (dwFileSize == INVALID_FILE_SIZE) {

		CloseHandle(mmf.hFile);
		return 2;
	}

	 mmf.hMappiing  = CreateFileMapping(mmf.hFile, nullptr, PAGE_READWRITE,
		0, 0, "Map");

	if (mmf.hMappiing == nullptr) {
	
		CloseHandle(mmf.hFile);
		return 3;
	}


	mmf.dataptr = (short*)MapViewOfFile(mmf.hMappiing,
		FILE_MAP_ALL_ACCESS,
		0,
		0,
		0);
	if (mmf.dataptr == nullptr) {
	
		CloseHandle(mmf.hMappiing);
		CloseHandle(mmf.hFile);
		return 4;

	}
	
	return 6;
	
	
	/*printf("File Map Created ---"); NL

		int f=10;

	if (1) {
		int f = 10;
	}
*/



		/*
		Причем касательно областей видимости обїявлять переменную в 
		Обїявлять переменніе можно в кейс и свитч
		*/

}

void OpenFile(const char type[], char name[]) {

	if ((b = fopen(name, type)) == NULL) {
		TB; perror("Error of read"); NL;
		exit(0);
	}



}

void Save(void) {
	while (getchar() != '\n') //Этот конструкт нужен что бы точно выпиать все символы какие могут быть в очереди
		;

	TB; puts("Enter Name of file>>"); NL;
	char name[100];
	if (b)fclose(b);

	gets_s(name);
	strcat(name, ".bin");
	OpenFile("wb", name);

	puts("Its Picture of Animation -- [1] Else -- [2]");
	switch (getch()){
	case '1': {
		fwrite(&ROWS, sizeof(short), 1, b);
		fwrite(&COLUMNS, sizeof(short), 1, b);


		for (register int i = 0; i < ROWS; i++)
			for (register int j = 0; j < COLUMNS; j++)
				fwrite(&picture[i][j], sizeof(short), 1, b);
	}break;

	case '2': {
		fwrite(&ZDimension, sizeof(short), 1, b);
		fwrite(&ROWS, sizeof(short), 1, b);
		fwrite(&COLUMNS, sizeof(short), 1, b);

		for(register int z=0;z<ZDimension;z++)
		for (register int i = 0; i < ROWS; i++)
			for (register int j = 0; j < COLUMNS; j++)
				fwrite(&WorldBox[z][i][j], sizeof(short), 1, b);
	

	
	}break;
}
	fclose(b);

}

short AnimHelp(Node *list,short Color,short i, short j, short *AnimaCount) {
	AddNode(list, i, ++(*AnimaCount));
	AddNode(list, j, ++(*AnimaCount));
	AddNode(list, Color,++(*AnimaCount));
	return 0;
}

void SaveAnimation(short ObjectCount, Node *list) {
	while (getchar() != '\n') 
		;
	TB; printf("Enter Name of file>>"); NL;
	char name[100];
	if (b)fclose(b);

	gets_s(name);
	strcat(name, ".bin");
	OpenFile("wb", name);


	short Num = (ObjectCount / 3);
	fwrite(&Num, sizeof(short), 1, b);

	for (register int i = 0; i < ObjectCount; i++) {
		Num = GetNode(list, i);
		fwrite(&Num, sizeof(short), 1, b);
	}
}

void AddNode(Node *list, short val, short counter) {

	Node *tmp = (Node*)calloc(1, sizeof(Node));
	Node *tmp_2 = (Node*)malloc(1*sizeof(Node));

	if (counter != 1) {

			tmp->value = val;
			tmp_2 = list;
			while (tmp_2->next)
				tmp_2 = tmp_2->next;

			tmp_2->next = (Node*)calloc(1, sizeof(Node));
			tmp_2->next = tmp;

		}

	if (counter == 1) {

		list->value=val;

	}
}

short GetNode(Node *list, short count) {
	Node *tmp = (Node*)calloc(1, sizeof(Node));
	if (!list)return 3;
	else {
		tmp = list;
		int i = 0;
		while (tmp->next&&i < count) {
			tmp = tmp->next;
			i++;
		}
	}
	return tmp->value;
}

void EditPicture(char type) {
	short i = 0, j = 0, z = 0;
	char c = 0;
	short typeOfCollor=300;

	short oldZ = 0;

	Node *list = (Node*)calloc(1, sizeof(Node));
	system("cls");
	short AnimaCount = 0;
	char index_mat = 0;
	//В силу офигенности системы рендерера мы видим как бы много, но как бы не очень


	PainterTransfer(i, j, ((type == 'W') ? 'w' : 's'), typeOfCollor, z);
	NL NL NL
		TB; puts(" \x1b[0;33;40m{8}\x1b[0;37;40m"); NL
		puts("\x1b[0;33;40m<-[4]{exit}[6]->\x1b[0;37;40m"); NL
		TB;	puts("\x1b[0;33;40m{2}\x1b[0;37;40m"); NL
		puts("\x1b[0;33;40mINFORMATION: if you enter:\n(b)=BLACK\t(r)=RED\t(g)=GREEN\t(y)=Yeellow\n(l)=Blue\t(p)=Purple\t(k)=light_blue\t(w)=White\t(z)=Grey\n\
(x)=mauve\t(c)=light_blue\t(v)=light_green\t(n)light_light_blue\n(m)=light_red\t(d)=light_yeellow\t(i)=light_white\x1bTransponent=(t)\nSollid_material(o)\x1b[0;37;40m");
	NL
		puts("---exit-->>[esc]"); NL
		puts("Set Position of ROWS->[=] "); NL
		puts("Set Positionof COLUMNS->[-]"); NL
		type == 'A' ? printf("Generate New Frame----[f]--Stop Full---[9]-\n") : NULL;
	  

	do {

		    type == 'A'&&typeOfCollor!=300? AnimHelp(list, typeOfCollor, i, j, &AnimaCount) :type=='W'?WorldBox[z][i][j]=typeOfCollor!=300?
				typeOfCollor:WorldBox[z][i][j]:picture[i][j] = typeOfCollor != 300?typeOfCollor:picture[i][j];

			

			PainterTransfer(i, j, oldZ!=z?'w':'r', typeOfCollor,z);
			oldZ != z ? oldZ = z : oldZ = oldZ;
			printf("\x1b[%s;0H", IntToString(ROWS + (type=='A'?18:16)));//Данная конструкция отвечает за перенос по оси игрек
			type == 'W'?printf("ZDimension--%d", z) :NULL;
			type == 'W' ? printf("ZDimension+::>>[7]::>>[1]      Objectlist:>> [o]\n") : NULL;
			if (type == 'W') {
				printf("Current index of list of Materials->[%d]", index_mat); NL
					printf("Current material--->[\x1b[0,33,43m%s\x1b[0,37,40m]", ObjectArray[index_mat]); NL
					puts("Locate object->>[a]");
				puts("Next Object->[e]");
				puts("Previous object->s]"); NL
			}
			switch (getch())
			{//считівает клавишу берет последний символ из буфера если буфер пуст ждет пока ченить появится без ехо вівода
			case'6': {j + 1 < COLUMNS ?++j : NULL;}break;
			case 't': {typeOfCollor = TRANSPONENT; }break;
			case '4': { j - 1 >= 0 ? --j : NULL; }break;
			case '8':{i - 1 >= 0 ? --i:NULL;}break;
			case '2': {i + 1 < ROWS ?++i:NULL;}break;
			case '7': {type=='W'?z + 1 < ZDimension ? ++z : NULL:NULL; }break;
			case '1': {type=='W'?z - 1 >= 0 ? --z : NULL:NULL; }break;
			case '9':{typeOfCollor = 300; }break;
			case'=': {Input(&i); }break;
			case'-': {Input(&j); }break;
			case 'b': {typeOfCollor = 0; }break;
			case'r': {typeOfCollor = 1;}break;
			case 'g': {typeOfCollor = 2;}break;
			case 'y': { typeOfCollor = 3;}break;
			case'l': { typeOfCollor = 4;}break;
			case 'p': {typeOfCollor = 5;}break;
			case 'k': {typeOfCollor = 6;}break;
			case 'w': {typeOfCollor = 7;}break;
			case 'z': {typeOfCollor = 12;}break;
			case 'x': {typeOfCollor = 11;}break;
			case'c': {typeOfCollor = 13;}break;
			case 'v': {typeOfCollor = 14;}break;
			case 'n': {typeOfCollor = 15;}break;
			case 'm': {typeOfCollor = 16;}break;
			case'd': {typeOfCollor = 17;}break;
			case 'i': {typeOfCollor = 18;}break;
			case'5': {type=='A'?SaveAnimation(AnimaCount,list):Save();	c = 1;}break;

			case 'o': {typeOfCollor = SOLID_MATERIAL; }break;

			case 'a': { type == 'W' ? typeOfCollor = ObjectArrayNum[index_mat] : NULL; }break;
			case 's': {type == 'W' ? index_mat - 1 >= 0 ? --index_mat : NULL : NULL; }break;
			case 'e': {type == 'W' ? index_mat + 1 < 16 ? index_mat++ : NULL : NULL; }break;

			case 27: {c = 1;}
			default: {  NL; ErrorOfEnter(); Pause(); }
			}
	} while (c != 1);
}

void Enter(const char type[], short *var) {
	printf("Enter Size of Picture: %s>>",type);
	short picture = 0;
	scanf("%hu", &picture);
	*var = picture;
}

void GenerateCanvas(void) 
{

	picture = (short**)calloc(ROWS+(ROWS*COLUMNS*sizeof(short)), sizeof(short*));


	if (picture == NULL) { //Проверка ошибки ыделения памяти также проверяем realoc free проверять не имеет смысла так как при free(NULL) ничего не произойдет просто 
		exit(EXIT_FAILURE);//Самый просто способ сразу вырубить программу после ошибки используя exit которая обрубает все и вся (Лучше сохранять что надо если вдруг но нам особо не надо)
	}

	picture[0] = (short*)(picture+ROWS);
	for (register int i = 1; i < ROWS; i++) {
		picture[i] = picture[0] + i * COLUMNS;
	}

	for (register int i = 0; i < ROWS; i++)
		for (register int j = 0; j < COLUMNS; j++)
			picture[i][j] = 7;


}

void GenerateWorld(void) {

	short **secondPointer, *firstpointer;

	WorldBox = (short***)calloc(COLUMNS + (ROWS*COLUMNS * sizeof(short*)) + (ROWS*COLUMNS*ZDimension * sizeof(short)), sizeof(short**));
	secondPointer = (short**)(WorldBox + COLUMNS);
	firstpointer = (short*)(WorldBox + COLUMNS + COLUMNS * ROWS);


	if (WorldBox == NULL) {
		exit(EXIT_FAILURE);
	}


	for (register short i = 0; i < ROWS; i++) {
		WorldBox[i] = secondPointer + i * ROWS;
		for (register short j = 0; j < COLUMNS; j++) {
			WorldBox[i][j] = firstpointer + j * (ROWS * ZDimension) + (i*ROWS*ZDimension*COLUMNS); ;
		}
	}

	
	for (register short z = 0; z < ZDimension; z++) {
		for (register int i = 0; i < ROWS; i++) {
			for (register int j = 0; j < COLUMNS; j++) {
				WorldBox[z][i][j] = 7;
				//printf("%d", WorldBox[z][i][j]);
			}
			//NL;
		}
		//NL; NL;
	}

}

void CreateNewPicture(void) {
	puts("It is will be 2D picture Or Animation -- [1]");
	puts("It is will be WorldBox -- [2]");
	puts("Return -- [3]");
	char flag = 0;
	do {
		switch (getch()) {
		case '1': {
			Enter("ROWS", &ROWS);
			Enter("COLUMNS", &COLUMNS);
			GenerateCanvas();
			flag = 1;
		}break;
		case '2': {
			Enter("ROWS", &ROWS);
			Enter("COLUMNS", &COLUMNS);
		//	Enter("Z-Dimension", &ZDimension);
			GenerateWorld(); 
			flag = 1;

		}break;

		case '3': {flag = 1; }break;
		default: {ErrorOfEnter(); }

		}
	} while (!flag);
}

void FileList(char type) {
	    system("cls");
		WIN32_FIND_DATA FindFileData;
		HANDLE hf;
		int flag = 0;
		hf = FindFirstFile
		("C:\\Users\\STARDESTROYER\\source\\repos\\PICTORE_EDITOR_V0.1\\PICTORE_EDITOR_V0.1\\*.bin", &FindFileData);

		if (hf != INVALID_HANDLE_VALUE) {

			do {
				flag++;
				printf("%s\n", FindFileData.cFileName);
			} while (FindNextFile(hf, &FindFileData) != 0);

			FindClose(hf);
		}
		else {
			printf("\t\t\x1b[4;31;40mEmpty\x1b[0;37;40m\n"); NL; Return();
		}
		if (flag == 0)printf("\t\t\x1b[4;31;40mEmpty\x1b[0;37;40m\n");
		else {
			printf("\t\t\x1b[0;33;40mWe have \x1b[0;35;40m{%d}\x1b[0;33;40m records\x1b[0;37;40m\n", flag);
			printf("\t\t\x1b[0;33;40m Enter Name of file\x1b[0;37;40m\n");
			scanf("%s", &name_of_file_to_add);
		}
		_getch();
	
		if (type == 'A') {
			
			printf("Chose>>Have Background 1-False/2-True");NL
			short chose = 0;
			Input(&chose);
			switch (chose) {
			case 1: {AnimationInit(0,NULL); }break;
			case 2: {
				char a[100];
				printf("Enter Str");NL
				scanf("%s", &a);
				AnimationInit(1, a); }break;
			}
		}

}


void Read(const char type) {

	if(b)fclose(b);

	//CreateFile(name_of_file_to_add, GENERIC_READ, FILE_SHARE_READ, NULL,
		//OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	OpenFile("rb", name_of_file_to_add);
	if (type == 'W'&&WorldBox)free(WorldBox);
	else if (picture)free(picture);

    GetMetaData(type);
	type=='W'?GenerateWorld():GenerateCanvas();


	if (type == 'W') {
		for(register int z=0;z<ZDimension;z++)
		for (register int i = 0; i < ROWS; i++) {
			for (register int j = 0; j < COLUMNS; j++)
				fread(&WorldBox[z][i][j], sizeof(short), 1, b);
		}
	}
	else {
		for (register int i = 0; i < ROWS; i++) {
			for (register int j = 0; j < COLUMNS; j++)
				fread(&picture[i][j], sizeof(short), 1, b);
		}
	}

}

void GetMetaData(const char type) 
{
	if (type == 'W') {
		fread(&ZDimension, sizeof(short), 1, b);
		fread(&ROWS, sizeof(short), 1, b);
		fread(&COLUMNS, sizeof(short), 1, b);
	}
	else {
		fread(&ROWS, sizeof(short), 1, b);
		fread(&COLUMNS, sizeof(short), 1, b);
	}
}

void Return(void) 
{
	 _getch();
	 system("cls"); //Функция очишает экран консоли
	 MainMenu();

}

void Input(short *chose) 
{

	if (scanf_s("%hu", chose) != 1) {

		scanf("%*[^\n]"); Return();
	}
}

short Input(void) {
	short chose = 0;
	if (scanf_s("%hu", &chose) != 1) {

		scanf("%*[^\n]"); Return();
	}
	return chose;
}

char* IntToString(int n) {

	char *s, c, *tempStr;
	int i = 0, j = n;
	s = (char*)calloc(40, sizeof(char));

	do {
		c = j % 10;
		j = j / 10;
		s[i] = c | 0x30;//Таким не хитрым (шутка на самом деле ваше не очевидным способом но не  суть)
						//мы получаем код нужного символа юмор состоит в том что старшие четыре бита символа числа едины и не делимы для всех 
						//а младшие четыре соответсвуют коду числа в двоичной форме. По сему  в данном случаи мы используем код числа "0" 
						//так как в ем мадшие биты равны нулю а старшие едины для всех чисел то есть мы впихиваем нужные нам для кода биты и хоп фокус
		i++;
	} while (j > 0);

	tempStr = (char*)calloc(i, sizeof(char));

	for (i = --i, j=0; i >= 0; i--,j++) {
		tempStr[j] = s[i];
	}
	tempStr[j] = '\0';
	
	return tempStr;

}

int StringToInt(const char *s) {
	
	int temp = 0;
	for (register int i = 0; s[i] >= 0x30 && s[i] <= 0x39; i++) {
		temp += s[i] & 0x0F; // В данно случае магия чсел состоит в том что мы получаем
		// число но не целиком а его кусочек (одну цифру) и казалось бы всё жизнь кончена но тут на помошь
		// приходит домножение на десять которое чудесным образом в итоге дает нам искомое число но 
		// с лишним нулем в конце а мы хоп и обрываем его поделив после цикла и получаем искомое число.
		temp *= 10;
	}
	temp /=10;
	
	return temp;
}

char* DoubleToStr(double n, int acuracy) {
	char *s, c, *tempStr;
	s = (char*)calloc(40, sizeof(char));

	double num;
	register int i, j;
	i = acuracy;
	//Вся эта пляска с бубном нужна так как мы сначала задаем точность
	//потом считаем сколько в целой части в итоге целое будет всегда
	//а после только часть
	j = 0;
	num = n;
	
	do { // преобразуем к нормированной форме (если не меньше 1)
		//проще говоря считаем сколько там цифр в целой части
		num /=10;
		i++;
	} while ((int)num > 0);

	num *= 10;// Тута мы вытягиваем из пучины забвения первое число

	for (; i > 0; i--, j++) {

		c = (int)num; // выделяем цифру, соответствующую целой части (первому числу)
		s[j] = c | 0x30;

		if (i == acuracy) { // определяем положение разделителя
			s[j] = '.';
			j++;
		}

		num -= (double)c; // вычитаем из числа целую часть в данном случаи использована 
		//дуалистичная природа типа char (хотя тут и так всё число) так что всё ок
		num *= 10; // переходим к следующему разряду (к следующему числу)
	}

	s[j] = '\0';j++;
	tempStr = (char*)calloc(j, sizeof(char)); // выделяем память под возвращаемую строку
	for (i = 0; i < j; i++) // формируем возвращаемую строку
		tempStr[i] = s[i];
	free(s);
	return tempStr ;
}

void EditTransfer(void) {
	short flag = 0;
	_getch();
	do {
		TB; printf("\x1b[0;33;40m////////EDIT PICTURE V0.1\\\\\\\x1b[0;37;40m"); NL;
		short chose = 0;
		printf("Start->(1)"); NL;
		TB; printf(">>Aniation->(2)"); NL;
			puts(">> WorldBox ->(3)"); NL;
			TB; printf(">>Return<-[4]"); NL;
		Input(&chose);
		switch (chose) {
		case 1: {EditPicture(' '); flag = 1; }break;
		case 2: {EditPicture('A'); flag = 1; }break;
		case 3: {EditPicture('W'); flag = 1; }
		case 4: {flag = 1; }break;
		default: {ErrorOfEnter(); }
		}
	} while (flag != 1);

}

void *Music(void *args) {

	char a[226] = { "open " };
	strcat(a, (const char*)args);
	char b[226] = { " type mpegvideo alias song1" };
	strcat(a, b);

		MCIERROR me = mciSendString(a, NULL, 0, 0);

		if (me == 0) {
		

			do {
				me = mciSendString("play song1 wait", NULL, 0, 0);
				mciSendString("close song1", NULL, 0, 0);
			} while (music);
		}
		return 0;
}

void PrintMap() {
	int zdimension = 0;
	system("cls");

	do {
		PainterTransfer(0, 0, 'w', 0, zdimension);
		Painter(ROWS, COLUMNS, zdimension);
		printf(">>>ROWS--%d .... >>> COLUMNS--%d", ROWS, COLUMNS); NL;
		printf("Change ZPosition ->2(down) :: 8(up) (Current -->%d)\n Exit-->3",zdimension);
		switch (getch()) {
		case '2': {zdimension - 1 >= 0 ? --zdimension : NULL; }break;
		case '8': {zdimension + 1 < ZDimension ? ++zdimension : NULL; }break;
		case '3': {return; }break;
		default: {ErrorOfEnter(); }
		}

	} while (1);
}



void Rewrite() {

	int offset = 2;
	for (register int i = 0; i < ROWS*COLUMNS; i++) {
		Sleep(2000);
		if (i != 0)offset = 0;
		mmf.dataptr[offset + i] = 7;
		

	}

}



void MainMenu(void) {
	do {

		TB; printf("\x1b[0;33;40m////////EDIT PICTURE V0.5\\\\\\\x1b[0;37;40m"); NL;
		
		TB; printf("\x1b[0;33;40mCREATE NEW PICTURE\x1b[0;37;40m>>1"); NL;
		TB; printf("\x1b[0;33;40mOpen file to Edit\x1b[0;37;40m>>2"); NL;
		TB; printf("\x1b[0;33;40mOpen Animation File\x1b[0;37;40m>>3"); NL;
		TB; printf("\x1b[0;33;40mView FILE\x1b[0;37;40m>>4"); NL;
		TB; printf("\x1b[0;33;40mClose Audio\x1b[0;37;40m>>5"); NL;
		TB; printf("\x1b[0;33;40mCreate World (2.5D (Simple))\x1b[0;37;40m>>6"); NL; //Генерация двумерной карты (С полом и потолком или без) Влияет на размер выделяемой памяти (либо мы создаем коробку либо нет)
		TB; puts("\x1b[0;33;40mLoad Level\x1b[0;37;40m>>7"); NL;
		TB; puts("\x1b[0;33;40mLoad Level\x1b[0;37;40m(\x1b[0;35;40mMap\x1b[0;37;40m)>>8"); NL;
		TB; printf("\x1b[0;33;40mEXIT\x1b[0;37;40m>>9 ........ 11 - Rewrite"); NL;

	



		//create = GetLastError();
	//	printf("Mapping ---> %d", create); NL;
		short chose = 0;
		Input(&chose);

	

		switch (chose) {
		case 1: {Sound(); CreateNewPicture(); EditTransfer(); system("cls"); }break;
		case 2: {Sound(); FileList(' '); 
			
			puts("It is picture -- 1  .... WorldBox -- 2");
			switch (getch()) {
			case '1': {	Read(); }break;
			case '2': {Read('W'); }break;
			default: {ErrorOfEnter(); }
			} EditTransfer(); system("cls");  }break;
		case 3: {Sound(); FileList('A'); Return(); system("cls"); }
		case 4: {Sound(); FileList(' '); Read(); fclose(b); create = CreateFileMap(); system("cls"); Painter(ROWS, COLUMNS);  NL printf("Mapping -- %d  Error - %d", create, GetLastError()); }break;
		case 5: {Sound(); music = 0; }break;
		case 6: {Sound(); CreateNewPicture(); EditPicture('W'); }break;
		//case 7: {Sound(); FileList('W'); Read('W'); Game(); }break;
		case 8: {Sound(); FileList('W'); Read('W'); PrintMap(); }break;
		case 9: {Sound();
			if (picture)free(picture);
			if (Animation)free(Animation);
			if (WorldBox)free(WorldBox);
			UnmapViewOfFile(mapStruct.hFile);
			CloseHandle(mapStruct.hMappiing);
			exit(0); }break;
		case 11: { Rewrite(); break; }
		default: {ErrorOfEnter(); }

		}
	} while (1);
}

void StartMusic(void) {

	puts("StartMuzlo ? 1-true / 0-false\n");
	do {
		switch (getch()) {
			/*
			Тут используются функции работы с потоками библиотеки pthread.h установка ее немного отдает танцами с бубном однако результат занятный
			*/
		case '1': {pthread_create(&thread, NULL, Music, (void*)"C:\\Users\\STARDESTROYER\\source\\repos\\PICTORE_EDITOR_V0.1\\Latz.mp3"); }break;
		case '0': {return; }break;
		default: {ErrorOfEnter(); }
		}
	} while (1);

}

int main(int argc, char *argv[]){

 //StartMusic();
  MainMenu();
  UnmapViewOfFile(mmf.dataptr);
  CloseHandle(mmf.hMappiing);
  CloseHandle(mmf.hFile);


		   return 0;
}


//#define SIZE 32 // Дефайн включает все что после строки (буьте внимательні так как туда может попасть и точка с запятой и что то еще (для определения они не нужні)
